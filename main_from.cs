﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdvancedHMIDrivers;
using CsvHelper;
using System.Globalization;
using System.IO;
using Microsoft.VisualBasic;


namespace KS_MV_SoftwareModbus
{

    public partial class main_from : Form
    {
        public AdvancedHMIDrivers.ModbusRTUCom modbusRTUCom1;

        string pathFolder;
        string fileName;
        string fullPath;

        public main_from()
        {
            InitializeComponent();
            modbusRTUCom1 = new AdvancedHMIDrivers.ModbusRTUCom();
        }

        private void isoCalc_cfg_Click(object sender, EventArgs e)
        {
            if (modbusRTUCom1.StationAddress != 0 &&
                !string.IsNullOrEmpty(modbusRTUCom1.PortName) &&
                modbusRTUCom1.BaudRate != 0 &&
                modbusRTUCom1.DataBits != 0 &&
                modbusRTUCom1.Parity != 0 &&
                modbusRTUCom1.StopBits != 0)
            {
                isoCalcCfg_form formISO_5167 = new isoCalcCfg_form(modbusRTUCom1);
                formISO_5167.ShowDialog();
            }
            else
                MessageBox.Show("Error: Open Port Failed!");
        }

        private void comSetup_btn_Click(object sender, EventArgs e)
        {
            comSetup_form fromCOM_Setup= new comSetup_form(modbusRTUCom1);
            fromCOM_Setup.ShowDialog();
        }

        private void about_btn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("KS-MV Soft\nVersion 1.0.0 Beta");
        }

        private void liveData_btn_Click(object sender, EventArgs e)
        {
            if (modbusRTUCom1.StationAddress != 0 &&
               !string.IsNullOrEmpty(modbusRTUCom1.PortName) &&
               modbusRTUCom1.BaudRate != 0 &&
               modbusRTUCom1.DataBits != 0 &&
               modbusRTUCom1.Parity != 0 &&
               modbusRTUCom1.StopBits != 0)
            {
                liveData_form liveDataNow_form = new liveData_form(modbusRTUCom1);
                liveDataNow_form.ShowDialog();
            }
            else
                MessageBox.Show("Error: Open Port Failed!");
        }

        private void dataLogView_btn_Click(object sender, EventArgs e)
        {

            if (modbusRTUCom1.StationAddress != 0 &&
                !string.IsNullOrEmpty(modbusRTUCom1.PortName) &&
                modbusRTUCom1.BaudRate != 0 &&
                modbusRTUCom1.DataBits != 0 &&
                modbusRTUCom1.Parity != 0 &&
                modbusRTUCom1.StopBits != 0)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.Description = "Select Folder to Save CSV File.";

                DialogResult diaRes;
                diaRes = fbd.ShowDialog();

                if (diaRes == System.Windows.Forms.DialogResult.OK)
                {
                    pathFolder = @fbd.SelectedPath;
                    Console.WriteLine(pathFolder);
                }
                else
                {
                    //                MessageBox.Show(diaRes.ToString());
                    return;
                }

                // InputBox Tutorial
                // https://www.youtube.com/watch?v=LEsl_psykrQ

                fileName = Interaction.InputBox("Enter Name of CSV File:", "Input", "DataLog001", 300, 200);
                if (fileName != "")
                {
                    Console.WriteLine(fileName);
                }
                else
                {
                    MessageBox.Show("Error: Empty File!");
                }
                // CSV tutorial
                // https://www.youtube.com/watch?v=6gfPeexF3vM
                var testlist = new List<TestDataModel>();

                ushort jumlahData = 0;
                try
                {
                    jumlahData = ushort.Parse(modbusRTUCom1.Read("U42951"));
                }
                catch (Exception errMbus)
                {
                    MessageBox.Show(errMbus.Message);
                    return; 
                }

                TestDataModel testData = new TestDataModel();

                testData.Diagnostic_DP = ushort.Parse(modbusRTUCom1.Read("U43004"));
                testData.Diagnostic_T = ushort.Parse(modbusRTUCom1.Read("U43005"));
                testData.Diagnostic_PS = ushort.Parse(modbusRTUCom1.Read("U43006"));
                testData.Diagnostic_Device = ushort.Parse(modbusRTUCom1.Read("U43007"));
                testData.Diagnostic_Calculation = ushort.Parse(modbusRTUCom1.Read("U43008"));

                testData.Compressibility_Natural_Gas = float.Parse(modbusRTUCom1.Read("F43009"));
                testData.Density = float.Parse(modbusRTUCom1.Read("F43011"));
                testData.Viscosity = float.Parse(modbusRTUCom1.Read("F43013"));
                testData.Isentropic_Exponent = float.Parse(modbusRTUCom1.Read("F43015"));
                testData.Reynold_Number = float.Parse(modbusRTUCom1.Read("F43017"));
                testData.Differential_Pressure = float.Parse(modbusRTUCom1.Read("F43019"));
                testData.Process_Temperature = float.Parse(modbusRTUCom1.Read("F43021"));
                testData.Upstream_Preassure = float.Parse(modbusRTUCom1.Read("F43023"));
                testData.Downstream_Preassure = float.Parse(modbusRTUCom1.Read("F43025"));
                testData.Spesific_Gravity = float.Parse(modbusRTUCom1.Read("F43027"));
                testData.Pressure_Loss = float.Parse(modbusRTUCom1.Read("F43029"));
                testData.Pressure_Loss_Coeff = float.Parse(modbusRTUCom1.Read("F43031"));
                testData.Velocity_of_Upstream = float.Parse(modbusRTUCom1.Read("F43033"));
                testData.Pipe_Size = float.Parse(modbusRTUCom1.Read("F43035"));
                testData.Plate_Size = float.Parse(modbusRTUCom1.Read("F43037"));
                testData.Beta = float.Parse(modbusRTUCom1.Read("F43039"));
                testData.Joule_Thomson_coefficient = float.Parse(modbusRTUCom1.Read("F43041"));
                testData.Cd = float.Parse(modbusRTUCom1.Read("F43043"));
                testData.Expansion_Factor = float.Parse(modbusRTUCom1.Read("F43045"));
                testData.Flow_Extension = float.Parse(modbusRTUCom1.Read("F43047"));
                testData.Mass_Flow_Rate = float.Parse(modbusRTUCom1.Read("F43049"));
                testData.Volume_Flow_Rate = float.Parse(modbusRTUCom1.Read("F43051"));

                testData.Logging_Ke = ushort.Parse(modbusRTUCom1.Read("U43053"));

                testData.Year = ushort.Parse(modbusRTUCom1.Read("U43054")); /*[2xxx] (timestamp)*/
                testData.Month = ushort.Parse(modbusRTUCom1.Read("U43055")); /*[1-12] (timestamp)*/
                testData.Day = ushort.Parse(modbusRTUCom1.Read("U43056")); /*[1-31] (timestamp)*/
                testData.Hour = ushort.Parse(modbusRTUCom1.Read("U43057")); /*[0-23] (timestamp)*/
                testData.Minute = ushort.Parse(modbusRTUCom1.Read("U43058")); /*[0-59] (timestamp)*/
                testData.Second = ushort.Parse(modbusRTUCom1.Read("U43059")); /*[0-59] (timestamp)*/
                testData.DayofWeek = ushort.Parse(modbusRTUCom1.Read("U43060")); /*[1-7] (timestamp)*/

                for (int i = 0; i < jumlahData; i++)
                {
                    string loadDataKe = Convert.ToString(i);

                    modbusRTUCom1.Write("U41901", loadDataKe);

                    Console.WriteLine("Write CSV " + loadDataKe);
                    testlist.Add(new TestDataModel()
                    {
                        Diagnostic_DP = testData.Diagnostic_DP,
                        Diagnostic_T = testData.Diagnostic_T,
                        Diagnostic_PS = testData.Diagnostic_PS,
                        Diagnostic_Device = testData.Diagnostic_Device,
                        Diagnostic_Calculation = testData.Diagnostic_Calculation,

                        Compressibility_Natural_Gas = testData.Compressibility_Natural_Gas,
                        Density = testData.Density,
                        Viscosity = testData.Viscosity,
                        Isentropic_Exponent = testData.Isentropic_Exponent,
                        Reynold_Number = testData.Reynold_Number,
                        Differential_Pressure = testData.Differential_Pressure,
                        Process_Temperature = testData.Process_Temperature,
                        Upstream_Preassure = testData.Upstream_Preassure,
                        Downstream_Preassure = testData.Downstream_Preassure,
                        Spesific_Gravity = testData.Spesific_Gravity,
                        Pressure_Loss = testData.Pressure_Loss,
                        Pressure_Loss_Coeff = testData.Pressure_Loss_Coeff,
                        Velocity_of_Upstream = testData.Velocity_of_Upstream,
                        Pipe_Size = testData.Pipe_Size,
                        Plate_Size = testData.Plate_Size,
                        Beta = testData.Beta,
                        Joule_Thomson_coefficient = testData.Joule_Thomson_coefficient,
                        Cd = testData.Cd,
                        Expansion_Factor = testData.Expansion_Factor,
                        Flow_Extension = testData.Flow_Extension,
                        Mass_Flow_Rate = testData.Mass_Flow_Rate,
                        Volume_Flow_Rate = testData.Volume_Flow_Rate,

                        Logging_Ke = testData.Logging_Ke,
                        
                        Year = testData.Year, /*[2xxx] (timestamp)*/
                        Month = testData.Month, /*[1-12] (timestamp)*/
                        Day = testData.Day, /*[1-31] (timestamp)*/
                        Hour = testData.Hour, /*[0-23] (timestamp)*/
                        Minute = testData.Minute, /*[0-59] (timestamp)*/
                        Second = testData.Second, /*[0-59] (timestamp)*/
                        DayofWeek = testData.DayofWeek, /*[1-7] (timestamp)*/
                    });
                }

                fullPath = pathFolder + "\\" + fileName + ".csv";
                Console.WriteLine(fullPath);

                var writer = new StreamWriter(fullPath);
                var csvwriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvwriter.WriteRecords(testlist);
                csvwriter.Dispose();
                writer.Dispose();

                string info = "Data has been saved in\n" + fullPath;
                MessageBox.Show(info);
            }
            else
                MessageBox.Show("Error: Open Port Failed!");
        }

        class TestDataModel
        {
            public ushort Diagnostic_DP { get; set; }
            public ushort Diagnostic_T { get; set; }
            public ushort Diagnostic_PS { get; set; }
            public ushort Diagnostic_Device { get; set; }
            public ushort Diagnostic_Calculation { get; set; }

            public float Compressibility_Natural_Gas { get; set; }
            public float Density { get; set; }
            public float Viscosity { get; set; }
            public float Isentropic_Exponent { get; set; }
            public float Reynold_Number { get; set; }
            public float Differential_Pressure { get; set; }
            public float Process_Temperature { get; set; }
            public float Upstream_Preassure { get; set; }
            public float Downstream_Preassure { get; set; }
            public float Spesific_Gravity { get; set; }
            public float Pressure_Loss { get; set; }
            public float Pressure_Loss_Coeff { get; set; }
            public float Velocity_of_Upstream { get; set; }
            public float Pipe_Size { get; set; }
            public float Plate_Size { get; set; }
            public float Beta { get; set; }
            public float Joule_Thomson_coefficient { get; set; }
            public float Cd { get; set; }
            public float Expansion_Factor{ get; set; }
            public float Flow_Extension { get; set; }
            public float Mass_Flow_Rate { get; set; }
            public float Volume_Flow_Rate { get; set; }

            public ushort Logging_Ke { get; set; }

            public ushort Year { get; set; } //[2xxx] (timestamp)
            public ushort Month { get; set; } //[1-12] (timestamp)
            public ushort Day { get; set; } //[1-31] (timestamp)
            public ushort Hour { get; set; } //[0-23] (timestamp)
            public ushort Minute { get; set; } //[0-59] (timestamp)
            public ushort Second { get; set; } //[0-59] (timestamp)
            public ushort DayofWeek { get; set; } //[1-7] (timestamp)
    }

        private void deviceTime_btn_Click(object sender, EventArgs e)
        {
            if (modbusRTUCom1.StationAddress != 0 &&
               !string.IsNullOrEmpty(modbusRTUCom1.PortName) &&
               modbusRTUCom1.BaudRate != 0 &&
               modbusRTUCom1.DataBits != 0 &&
               modbusRTUCom1.Parity != 0 &&
               modbusRTUCom1.StopBits != 0)
            {
                dateTime_form dateTimeForm = new dateTime_form(modbusRTUCom1);
                dateTimeForm.ShowDialog();
            }
            else
                MessageBox.Show("Error: Open Port Failed!");
        }
    }
}
