﻿
namespace KS_MV_SoftwareModbus
{
    partial class comSetup_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboport = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.stopBits_comBox = new System.Windows.Forms.ComboBox();
            this.parity_comBox = new System.Windows.Forms.ComboBox();
            this.slvID_txtBox = new System.Windows.Forms.TextBox();
            this.baud_comBox = new System.Windows.Forms.ComboBox();
            this.dataBits_comBox = new System.Windows.Forms.ComboBox();
            this.openPort_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(110, 245);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 35);
            this.button2.TabIndex = 2;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Baud";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Data Bits";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Parity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(59, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Stop Bits";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Slave ID";
            // 
            // cboport
            // 
            this.cboport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboport.FormattingEnabled = true;
            this.cboport.Location = new System.Drawing.Point(129, 40);
            this.cboport.Name = "cboport";
            this.cboport.Size = new System.Drawing.Size(121, 24);
            this.cboport.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(89, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Port";
            // 
            // stopBits_comBox
            // 
            this.stopBits_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stopBits_comBox.FormattingEnabled = true;
            this.stopBits_comBox.Items.AddRange(new object[] {
            "1",
            "2"});
            this.stopBits_comBox.Location = new System.Drawing.Point(129, 160);
            this.stopBits_comBox.Name = "stopBits_comBox";
            this.stopBits_comBox.Size = new System.Drawing.Size(121, 24);
            this.stopBits_comBox.TabIndex = 14;
            // 
            // parity_comBox
            // 
            this.parity_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.parity_comBox.FormattingEnabled = true;
            this.parity_comBox.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even"});
            this.parity_comBox.Location = new System.Drawing.Point(129, 130);
            this.parity_comBox.Name = "parity_comBox";
            this.parity_comBox.Size = new System.Drawing.Size(121, 24);
            this.parity_comBox.TabIndex = 13;
            // 
            // slvID_txtBox
            // 
            this.slvID_txtBox.Location = new System.Drawing.Point(129, 12);
            this.slvID_txtBox.Name = "slvID_txtBox";
            this.slvID_txtBox.Size = new System.Drawing.Size(121, 22);
            this.slvID_txtBox.TabIndex = 16;
            // 
            // baud_comBox
            // 
            this.baud_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.baud_comBox.FormattingEnabled = true;
            this.baud_comBox.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "230400",
            "256000"});
            this.baud_comBox.Location = new System.Drawing.Point(129, 70);
            this.baud_comBox.Name = "baud_comBox";
            this.baud_comBox.Size = new System.Drawing.Size(121, 24);
            this.baud_comBox.TabIndex = 17;
            // 
            // dataBits_comBox
            // 
            this.dataBits_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataBits_comBox.FormattingEnabled = true;
            this.dataBits_comBox.Items.AddRange(new object[] {
            "7",
            "8"});
            this.dataBits_comBox.Location = new System.Drawing.Point(129, 100);
            this.dataBits_comBox.Name = "dataBits_comBox";
            this.dataBits_comBox.Size = new System.Drawing.Size(121, 24);
            this.dataBits_comBox.TabIndex = 18;
            // 
            // openPort_btn
            // 
            this.openPort_btn.Location = new System.Drawing.Point(110, 203);
            this.openPort_btn.Name = "openPort_btn";
            this.openPort_btn.Size = new System.Drawing.Size(104, 36);
            this.openPort_btn.TabIndex = 19;
            this.openPort_btn.Text = "Open Port";
            this.openPort_btn.UseVisualStyleBackColor = true;
            this.openPort_btn.Click += new System.EventHandler(this.button1_Click);
            // 
            // comSetup_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 292);
            this.Controls.Add(this.openPort_btn);
            this.Controls.Add(this.dataBits_comBox);
            this.Controls.Add(this.baud_comBox);
            this.Controls.Add(this.slvID_txtBox);
            this.Controls.Add(this.stopBits_comBox);
            this.Controls.Add(this.parity_comBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboport);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Name = "comSetup_form";
            this.Text = "COM Setup";
            this.Load += new System.EventHandler(this.comSetup_form_load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboport;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox stopBits_comBox;
        private System.Windows.Forms.ComboBox parity_comBox;
        private System.Windows.Forms.TextBox slvID_txtBox;
        private System.Windows.Forms.ComboBox baud_comBox;
        private System.Windows.Forms.ComboBox dataBits_comBox;
        private System.Windows.Forms.Button openPort_btn;
    }
}