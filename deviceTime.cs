﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdvancedHMIDrivers;

namespace KS_MV_SoftwareModbus
{
    public partial class dateTime_form : Form
    {
        public AdvancedHMIDrivers.ModbusRTUCom mbus1;

        public ushort jumChar = 7;

        public dateTime_form(AdvancedHMIDrivers.ModbusRTUCom mbus)
        {
            InitializeComponent();
            mbus1 = mbus;
        }

        private void readTime_btn_Click(object sender, EventArgs e)
        {
 

            string[] stringRecv = new string[jumChar];
            char[] chars = new char[jumChar];
            ushort[] dat16 = new ushort[jumChar];

            ushort Day, Month, Year, Hour, Minute;
            try
            {
                numericUpDown_day.Value = ushort.Parse(mbus1.Read("U42901"));
            }
            catch (Exception errMbus)
            {
                MessageBox.Show(errMbus.Message);
                return;
            }

            numericUpDown_day.Value = ushort.Parse(mbus1.Read("U42901"));
            numericUpDown_month.Value = ushort.Parse(mbus1.Read("U42902"));
            numericUpDown_year.Value = ushort.Parse(mbus1.Read("U42903"));
            numericUpDown_hour.Value = ushort.Parse(mbus1.Read("U42904"));
            numericUpDown_minute.Value = ushort.Parse(mbus1.Read("U42905"));
        }

        private void setTime_btn_Click(object sender, EventArgs e)
        {

            string[] stringRecv = new string[jumChar];

            stringRecv[0] = Convert.ToString(numericUpDown_day.Value);
            stringRecv[1] = Convert.ToString(numericUpDown_month.Value);
            stringRecv[2] = Convert.ToString(numericUpDown_year.Value);
            stringRecv[3] = Convert.ToString(numericUpDown_hour.Value);
            stringRecv[4] = Convert.ToString(numericUpDown_minute.Value);

            try
            {
                mbus1.Write("U42901", stringRecv);
            }
            catch (Exception errMbus)
            {
                MessageBox.Show(errMbus.Message);
                return;
            }
        }
    }
}
