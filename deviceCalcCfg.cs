﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KS_MV_SoftwareModbus
{
    public partial class isoCalcCfg_form : Form
    {
        public AdvancedHMIDrivers.ModbusRTUCom mbus1;
        Exception showErrorMbus;
        public ushort Fluid_Property_Register = 0;
        public ushort Flow_Rate_Calculation_Register = 0;

        ushort Wet_Correction = 0;
        ushort Flow_Rate_Calculation = 0;
        ushort jumChar = 30;

        public isoCalcCfg_form(AdvancedHMIDrivers.ModbusRTUCom mbus)
        {
            InitializeComponent();
            mbus1 = mbus;
        }

        public void Write_Flow_Run_Configuration()
        {
            
            string sentence = tagName_txtBox.Text;
            char[] charArr = sentence.ToCharArray();
            string[] dataSend = new string[jumChar];
            ushort[] dat16 = new ushort[jumChar];

            int x = 0;
            foreach (char ch in charArr)
            {
                dat16[x] = (ushort)ch;
                dataSend[x] = Convert.ToString(dat16[x]);
                x++;
            }
            mbus1.Write("U41201", dataSend);
        }

        public int Read_Flow_Run_Configuration()
        {
            int errCode = 0;

            while (true)
            {
                
                string[] stringRecv = new string[jumChar];
                char[] chars = new char[jumChar];

                try
                {
                    stringRecv = mbus1.Read("U41201", jumChar);
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }


                for (int x=0; x < jumChar; x++)
                {
                    ushort dat16 = ushort.Parse(stringRecv[x]);
                    if (dat16 > 255) dat16 = 0;
                    chars[x] = (char)dat16;
                }

                string TagName = new string(chars);
                tagName_txtBox.Text = TagName;

                try
                {
                    Flow_Rate_Calculation_Register = ushort.Parse(mbus1.Read("U42001"));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    Fluid_Property_Register = ushort.Parse(mbus1.Read("U42002"));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    failVal_txtBox.Text = mbus1.Read("F42005");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    pipeSize_txtBox.Text = mbus1.Read("F42007");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    plateSize_txtBox.Text = mbus1.Read("F42009");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    tapDistUp_txtBox.Text = mbus1.Read("F42011");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    tapDistDown_txtBox.Text = mbus1.Read("F42013");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    isenExp_txtBox.Text = mbus1.Read("F42015");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    visco_txtBox.Text = mbus1.Read("F42017");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    specGrav_txtBox.Text = mbus1.Read("F42019");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    atmPress_txtBox.Text = mbus1.Read("F42021");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    velo_txtBox.Text = mbus1.Read("F42023");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    dens_txtBox.Text = mbus1.Read("F42025");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    unitScale_txtBox.Text = mbus1.Read("F42071");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                try
                {
                    unitOffset_txtBox.Text = mbus1.Read("F42073");
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;
                    break;
                }

                break;
            }
            if (errCode == 1)
            {
                MessageBox.Show(showErrorMbus.Message);
                this.Close();
            }
            else
            {

                ushort Fluid_Property_Calc = this.getFluidPropVal(Fluid_Property_Register);
                if (Fluid_Property_Calc > 2)
                {
                    MessageBox.Show("Error Not Supported\n" +
                        "Fluid Property Calculation Mode!\n" +
                        "Must Be AGA-8(1994)Detail Method\n" +
                        "AGA - 8(1994)GERG008 Method\n" +
                        "Fluid Library");

                    Fluid_Property_Calc = 2;
                }
                FluidPropCalc_comBox.SelectedIndex = Fluid_Property_Calc;

                if (this.CheckBit(Fluid_Property_Register, 15) == 1)
                    veloSelect_comBox.SelectedIndex = 1;
                else
                    veloSelect_comBox.SelectedIndex = 0;

                if (this.CheckBit(Fluid_Property_Register, 14) == 1)
                    densitySelect_comBox.SelectedIndex = 1;
                else
                    densitySelect_comBox.SelectedIndex = 0;

                if (this.CheckBit(Fluid_Property_Register, 13) == 1)
                    viscoSelect_comBox.SelectedIndex = 1;
                else
                    viscoSelect_comBox.SelectedIndex = 0;

                if (this.CheckBit(Fluid_Property_Register, 12) == 1)
                    isentropicSelect_comBox.SelectedIndex = 1;
                else
                    isentropicSelect_comBox.SelectedIndex = 0;

                this.getFlowRateCalcMode(Flow_Rate_Calculation_Register, ref Wet_Correction, ref Flow_Rate_Calculation);
                // just one support mode
                if (Wet_Correction > 0) Wet_Correction = 0;
                if (Flow_Rate_Calculation > 0) Flow_Rate_Calculation = 0;

                iso5167calc_radiobtn.Checked = true;
                noWetCor_radiobtn.Checked = true;

                
            }

            return errCode;
        }

        public void getFlowRateCalcMode(ushort value,ref ushort WetCorr, ref ushort FlowRateCalc)
        {
            /*
             * 15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
             *                  |   |   |   |               |   |   |   |   |
             *                  Wet Correction		        Flow Rate Calculation
             */
            WetCorr = (ushort)((value & 0b0000111100000000)>>8);
            FlowRateCalc = (ushort)(value & 0b0000000000011111);
        }

        public ushort getFluidPropVal(ushort value)
        {
            /*
             * 15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
             * |    |   |   |                               |   |   |   |   |
             * VE	DE	VI	Isen			                Fluid Property Calc
             */
            return (ushort)(value & 0b0000000000011111);
        }

        public ushort TurnBitOn(ushort value, ushort bitToTurnOn)
        {
            return (ushort)(value | 1 << bitToTurnOn);
        }

        public ushort CheckBit(ushort value, ushort num_bit)
        {
            ushort temp = (ushort)(value & (1 << num_bit));
            ushort bit_val = (ushort)(temp >> num_bit);
            return bit_val;
        }

        public ushort TurnBitOff(ushort value, ushort bitToTurnOff)
        {
            return (ushort)(value & ~ (1 << bitToTurnOff));
        }

        private void isoCalcCfg_form_Load(object sender, EventArgs e)
        {
            this.Read_Flow_Run_Configuration();
        }

        private void readKS_MVCfg_btn_Click(object sender, EventArgs e)
        {
            if(this.Read_Flow_Run_Configuration() == 0)
                MessageBox.Show("Read Config OK.");
        }

        private void FluidPropCalc_comBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(String.Equals("Fluid Library", FluidPropCalc_comBox.Text))
                fluidName_comBox.Enabled = true;
            else
                fluidName_comBox.Enabled = false;
        }

        private void saveKS_MVCfg_btn_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to send this config ??",
                                     "Confirm Send!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                this.Write_Flow_Run_Configuration();
                mbus1.Write("40101", "10001");
            }
        }

        private void setDef_comBox_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to Set Default this config ??",
                                     "Confirm Set Default!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes) {
                mbus1.Write("40101", "10003");
                this.Read_Flow_Run_Configuration();
            }
        }
    }
}
