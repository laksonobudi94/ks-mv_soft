﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace KS_MV_SoftwareModbus
{
    public partial class liveData_form : Form
    {
        AdvancedHMIDrivers.ModbusRTUCom mbus1;
        Exception showErrorMbus;

        Thread t;
        public static Semaphore _pool;
        int nTasks = 0;

        object o = 0;
        public liveData_form(AdvancedHMIDrivers.ModbusRTUCom mbus)
        {
            InitializeComponent();
            t = new Thread(new ThreadStart(ThreadProc));
            // Create a semaphore that can satisfy up to three
            // concurrent requests. Use an initial count of zero,
            // so that the entire semaphore count is initially
            // owned by the main program thread.
            //
            _pool = new Semaphore(0, 3);

            mbus1 = mbus;
            runStatus = false;
            wantExit = false;
        }



        int jumChar = 30;
        ushort Fluid_Property_Register = 0;
        ushort Flow_Rate_Calculation_Register = 0;
        ushort Wet_Correction = 0;
        ushort Flow_Rate_Calculation = 0;
        string tmp;
        bool runStatus = false;
        bool wantExit = false;
        public ushort getFluidPropVal(ushort value)
        {
            /*
             * 15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
             * |    |   |   |                               |   |   |   |   |
             * VE	DE	VI	Isen			                Fluid Property Calc
            */
            return (ushort)(value & 0b0000000000011111);
        }

        public void getFlowRateCalcMode(ushort value, ref ushort WetCorr, ref ushort FlowRateCalc)
        {
            /*
             * 
             * 15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
             *                  |   |   |   |               |   |   |   |   |
             *                  Wet Correction		        Flow Rate Calculation
            */
            WetCorr = (ushort)(value & 0b0000111100000000);
            FlowRateCalc = (ushort)(value & 0b0000000000011111);
        }

        public int Read_LiveData()
        {
            int errCode = 0;

            string[] stringRecv = new string[jumChar];
            char[] chars = new char[jumChar];

            while (true)
            {
                if(errCode == 1)
                {
                    Console.WriteLine("Exit From Thread...");
                    break;
                }
                Console.WriteLine("Read Modbus Data...");

                try
                {
                    stringRecv = mbus1.Read("U41201", jumChar);
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                for (int x = 0; x < jumChar; x++)
                {
                    ushort dat16 = ushort.Parse(stringRecv[x]);
                    if (dat16 > 255) dat16 = 0;
                    chars[x] = (char)dat16;
                }

                string TagName = new string(chars);
                tagNameLiveDat_txtBox.Invoke((MethodInvoker)(() => tagNameLiveDat_txtBox.Text = TagName));

                try
                {
                    Flow_Rate_Calculation_Register = ushort.Parse(mbus1.Read("U42001"));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    Fluid_Property_Register = ushort.Parse(mbus1.Read("U42002"));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                ushort Fluid_Property_Calc = this.getFluidPropVal(Fluid_Property_Register);
                if (Fluid_Property_Calc > 2)
                {
                    MessageBox.Show("Error Not Supported\n" +
                        "Fluid Property Calculation Mode!\n" +
                        "Must Be AGA-8(1994)Detail Method\n" +
                        "AGA - 8(1994)GERG008 Method\n" +
                        "Fluid Library");

                    Fluid_Property_Calc = 2;
                }

                if (Fluid_Property_Calc == 0)
                    fluidPropCalc_txtBox.Invoke((MethodInvoker)(() => fluidPropCalc_txtBox.Text = "AGA - 8(1994)Detail Method"));
                else if (Fluid_Property_Calc == 1)
                    fluidPropCalc_txtBox.Invoke((MethodInvoker)(() => fluidPropCalc_txtBox.Text = "AGA - 8(1994)GERG008 Method"));
                else if (Fluid_Property_Calc == 2)
                {
                    fluidPropCalc_txtBox.Invoke((MethodInvoker)(() => fluidPropCalc_txtBox.Text = "Fluid Library"));

                    try
                    {
                        tmp = mbus1.Read("U42004");
                    }
                    catch (Exception errMbus)
                    {
                        errCode = 1;
                        MessageBox.Show(errMbus.Message);
                        this.Invoke((MethodInvoker)(() => this.Close()));
                        return errCode;
                    }

                    Console.WriteLine("Cek Fluid Name List...");
                    ushort numFluid = 0;
                    if (tmp != null)
                        numFluid = ushort.Parse(tmp);
                    fluidName_comBox.Invoke((MethodInvoker)(() => fluidName_comBox.SelectedIndex = numFluid));
                    Console.WriteLine("Show Fluid Name List...");

                }

                this.getFlowRateCalcMode(Flow_Rate_Calculation_Register, ref Wet_Correction, ref Flow_Rate_Calculation);

                // just one support mode
                if (Wet_Correction >= 0)
                {
                    wetCor_txtBox.Invoke((MethodInvoker)(() => wetCor_txtBox.Text = "No Wet Correction"));
                    Wet_Correction = 0;
                }
                if (Flow_Rate_Calculation >= 0)
                {
                    modeCalc_txtBox.Invoke((MethodInvoker)(() => modeCalc_txtBox.Text = "ISO 5167 : 2003 Orifice Flow"));
                    Flow_Rate_Calculation = 0;
                }

                try
                {
                    compress_txtBox.Invoke((MethodInvoker)(() => compress_txtBox.Text = mbus1.Read("F44009")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    dens_txtBox.Invoke((MethodInvoker)(() => dens_txtBox.Text = mbus1.Read("F44011")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    visco_txtBox.Invoke((MethodInvoker)(() => visco_txtBox.Text = mbus1.Read("F44013")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    isentro_txtBox.Invoke((MethodInvoker)(() => isentro_txtBox.Text = mbus1.Read("F44015")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    reynold_txtBox.Invoke((MethodInvoker)(() => reynold_txtBox.Text = mbus1.Read("F44017")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    diffPress_txtBox.Invoke((MethodInvoker)(() => diffPress_txtBox.Text = mbus1.Read("F44019")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    procTemp_txtBox.Invoke((MethodInvoker)(() => procTemp_txtBox.Text = mbus1.Read("F44021")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    upPress_txtBox.Invoke((MethodInvoker)(() => upPress_txtBox.Text = mbus1.Read("F44023")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    downPress_txtBox.Invoke((MethodInvoker)(() => downPress_txtBox.Text = mbus1.Read("F44025")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    specGrav_txtBox.Invoke((MethodInvoker)(() => specGrav_txtBox.Text = mbus1.Read("F44027")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    pressLoss_txtBox.Invoke((MethodInvoker)(() => pressLoss_txtBox.Text = mbus1.Read("F44029")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    pressLossCoef_txtBox.Invoke((MethodInvoker)(() => pressLossCoef_txtBox.Text = mbus1.Read("F44031")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    velo_txtBox.Invoke((MethodInvoker)(() => velo_txtBox.Text = mbus1.Read("F44033")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    pipeSize_txtBox.Invoke((MethodInvoker)(() => pipeSize_txtBox.Text = mbus1.Read("F44035")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    plateSize_txtBox.Invoke((MethodInvoker)(() => plateSize_txtBox.Text = mbus1.Read("F44037")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    beta_txtBox.Invoke((MethodInvoker)(() => beta_txtBox.Text = mbus1.Read("F44039")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    jtCoef_txtBox.Invoke((MethodInvoker)(() => jtCoef_txtBox.Text = mbus1.Read("F44041")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    cd_txtBox.Invoke((MethodInvoker)(() => cd_txtBox.Text = mbus1.Read("F44043")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    expFact_txtBox.Invoke((MethodInvoker)(() => expFact_txtBox.Text = mbus1.Read("F44045")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    flowExt_txtBox.Invoke((MethodInvoker)(() => flowExt_txtBox.Text = mbus1.Read("F44047")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    massFlow_txtBox.Invoke((MethodInvoker)(() => massFlow_txtBox.Text = mbus1.Read("F44049")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    volumeFlow_txtBox.Invoke((MethodInvoker)(() => volumeFlow_txtBox.Text = mbus1.Read("F44051")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                try
                {
                    logKe_txtBox.Invoke((MethodInvoker)(() => logKe_txtBox.Text = mbus1.Read("U44053")));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                int jumTime = 6;
                string[] timeStamp = new string[jumTime];
                try
                {
                    timeStamp = mbus1.Read("U44054",6);
                    timeStamp_txtBox.Invoke((MethodInvoker)(() => timeStamp_txtBox.Text = string.Format(timeStamp[0] + "/" + timeStamp[1] + "/" + timeStamp[2] + "  " 
                                                                                            + timeStamp[3] + ":" + timeStamp[4] + ":" + timeStamp[5])));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }

                 string[] timeNowDevice = new string[jumTime];
                try
                {
                    //timeNowDevice = mbus1.Read("U42901", 6);
                    timeNowDevice_txtBox.Invoke((MethodInvoker)(() => timeNowDevice_txtBox.Text = string.Format(timeNowDevice[0] + "/" + timeNowDevice[1] + "/" + timeNowDevice[2] + "  "
                                                                                            + timeNowDevice[3] + ":" + timeNowDevice[4] + ":" + timeNowDevice[5])));
                }
                catch (Exception errMbus)
                {
                    showErrorMbus = errMbus;
                    errCode = 1;

                    break;
                }


                break;
            }

            if (errCode == 1)
            {
                Console.WriteLine("Error Modbus Read 2!");
                MessageBox.Show(showErrorMbus.Message);
                return errCode;
            }

            Console.WriteLine("Close Thread Read Modbus Data...");
            return errCode;
        }

        public void ThreadProc()
        {
            int i = 0;
            int miliSec_MbusRead = 500;

            while (true)
            {
                Monitor.Enter(o);

                int status = this.Read_LiveData();

                Console.WriteLine("status = {0}", status);
                if (status == 1)
                    break;

                // The main thread starts out holding the entire
                // semaphore count. Calling Release(3) brings the 
                // semaphore count back to its maximum value, and
                // allows the waiting threads to enter the semaphore,
                // up to three at a time.
                //
                Console.WriteLine("WantExit = {0}",wantExit);
                if (wantExit == true)
                {
                    Console.WriteLine("Main thread calls Release.");
                    //   _pool.Release(3);
                    Monitor.Pulse(o);
                }
                Monitor.Exit(o);
                
                Thread.Sleep(miliSec_MbusRead);
            } 
        }

        private void exitLiveData_btn_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure want to exit ??",
                         "Confirm Exit!!",
                         MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                wantExit = true;
                Console.WriteLine("Wait Semaphore...");
                //                _pool.WaitOne();
                while (Monitor.IsEntered(o)) {
                    Console.WriteLine("Wait Enter...");
                }
                Console.WriteLine("ThreadState Before => {0}", t.ThreadState);
                Console.WriteLine("Abort Thread!");
                try
                {
                    t.Abort();
                }
                catch(Exception Err)
                {
                    MessageBox.Show(Err.Message);
                }

                Console.WriteLine("ThreadState After => {0}", t.ThreadState);
                runStatus = false;

                this.Close();
            }
        }

        private void runLiveData_btn_Click(object sender, EventArgs e)
        {

            Console.WriteLine("Main thread: Start a second thread.");

            if (runStatus == false)
            {
                t.Start();
                runStatus = true;
            }

            Console.WriteLine("Main thread: ThreadProc.Join has returned.  Press Enter to end program.");
            Console.ReadLine();
        }

        private void liveData_formClosing(object sender, FormClosingEventArgs e)
        {
            Console.WriteLine("Closing event");
        }
    }
}
