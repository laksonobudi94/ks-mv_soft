﻿
namespace KS_MV_SoftwareModbus
{
    partial class isoCalcCfg_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dens_txtBox = new System.Windows.Forms.TextBox();
            this.velo_txtBox = new System.Windows.Forms.TextBox();
            this.atmPress_txtBox = new System.Windows.Forms.TextBox();
            this.specGrav_txtBox = new System.Windows.Forms.TextBox();
            this.visco_txtBox = new System.Windows.Forms.TextBox();
            this.readKS_MVCfg_btn = new System.Windows.Forms.Button();
            this.saveKS_MVCfg_btn = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.isenExp_txtBox = new System.Windows.Forms.TextBox();
            this.tapDistDown_txtBox = new System.Windows.Forms.TextBox();
            this.tapDistUp_txtBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.plateSize_txtBox = new System.Windows.Forms.TextBox();
            this.pipeSize_txtBox = new System.Windows.Forms.TextBox();
            this.failVal_txtBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.unitOffset_txtBox = new System.Windows.Forms.TextBox();
            this.unitScale_txtBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.setDef_comBox = new System.Windows.Forms.Button();
            this.iso5167calc_radiobtn = new System.Windows.Forms.RadioButton();
            this.noWetCor_radiobtn = new System.Windows.Forms.RadioButton();
            this.FluidPropCalc_comBox = new System.Windows.Forms.ComboBox();
            this.fluidName_comBox = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.ethernetIPforCLXCom1 = new AdvancedHMIDrivers.EthernetIPforCLXCom(this.components);
            this.veloSelect_comBox = new System.Windows.Forms.ComboBox();
            this.densitySelect_comBox = new System.Windows.Forms.ComboBox();
            this.viscoSelect_comBox = new System.Windows.Forms.ComboBox();
            this.isentropicSelect_comBox = new System.Windows.Forms.ComboBox();
            this.tagName_txtBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetIPforCLXCom1)).BeginInit();
            this.SuspendLayout();
            // 
            // dens_txtBox
            // 
            this.dens_txtBox.Location = new System.Drawing.Point(354, 368);
            this.dens_txtBox.Name = "dens_txtBox";
            this.dens_txtBox.Size = new System.Drawing.Size(64, 22);
            this.dens_txtBox.TabIndex = 76;
            // 
            // velo_txtBox
            // 
            this.velo_txtBox.Location = new System.Drawing.Point(354, 340);
            this.velo_txtBox.Name = "velo_txtBox";
            this.velo_txtBox.Size = new System.Drawing.Size(64, 22);
            this.velo_txtBox.TabIndex = 75;
            // 
            // atmPress_txtBox
            // 
            this.atmPress_txtBox.Location = new System.Drawing.Point(354, 312);
            this.atmPress_txtBox.Name = "atmPress_txtBox";
            this.atmPress_txtBox.Size = new System.Drawing.Size(64, 22);
            this.atmPress_txtBox.TabIndex = 74;
            // 
            // specGrav_txtBox
            // 
            this.specGrav_txtBox.Location = new System.Drawing.Point(354, 284);
            this.specGrav_txtBox.Name = "specGrav_txtBox";
            this.specGrav_txtBox.Size = new System.Drawing.Size(64, 22);
            this.specGrav_txtBox.TabIndex = 73;
            // 
            // visco_txtBox
            // 
            this.visco_txtBox.Location = new System.Drawing.Point(354, 256);
            this.visco_txtBox.Name = "visco_txtBox";
            this.visco_txtBox.Size = new System.Drawing.Size(64, 22);
            this.visco_txtBox.TabIndex = 72;
            // 
            // readKS_MVCfg_btn
            // 
            this.readKS_MVCfg_btn.Location = new System.Drawing.Point(497, 403);
            this.readKS_MVCfg_btn.Name = "readKS_MVCfg_btn";
            this.readKS_MVCfg_btn.Size = new System.Drawing.Size(97, 43);
            this.readKS_MVCfg_btn.TabIndex = 71;
            this.readKS_MVCfg_btn.Text = "Read Config";
            this.readKS_MVCfg_btn.UseVisualStyleBackColor = true;
            this.readKS_MVCfg_btn.Click += new System.EventHandler(this.readKS_MVCfg_btn_Click);
            // 
            // saveKS_MVCfg_btn
            // 
            this.saveKS_MVCfg_btn.Location = new System.Drawing.Point(600, 403);
            this.saveKS_MVCfg_btn.Name = "saveKS_MVCfg_btn";
            this.saveKS_MVCfg_btn.Size = new System.Drawing.Size(97, 43);
            this.saveKS_MVCfg_btn.TabIndex = 70;
            this.saveKS_MVCfg_btn.Text = "Send Config";
            this.saveKS_MVCfg_btn.UseVisualStyleBackColor = true;
            this.saveKS_MVCfg_btn.Click += new System.EventHandler(this.saveKS_MVCfg_btn_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(678, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 20);
            this.label18.TabIndex = 69;
            this.label18.Text = "Fluid Property";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(509, 208);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(169, 17);
            this.label17.TabIndex = 67;
            this.label17.Text = "Fluid Property Calculation";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(484, 177);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(194, 17);
            this.label16.TabIndex = 66;
            this.label16.Text = "Isentropic Exponent Selection";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(561, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 17);
            this.label15.TabIndex = 64;
            this.label15.Text = "Density Selection";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(553, 146);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(125, 17);
            this.label14.TabIndex = 62;
            this.label14.Text = "Viscosity Selection";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(559, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 17);
            this.label13.TabIndex = 60;
            this.label13.Text = "Velocity Selection";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(234, 373);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 17);
            this.label12.TabIndex = 58;
            this.label12.Text = "Density (kg/m^3)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(233, 345);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 17);
            this.label11.TabIndex = 57;
            this.label11.Text = "FR Velocity (m/s)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(148, 317);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 17);
            this.label10.TabIndex = 56;
            this.label10.Text = "FR Atmospheric Pressure (Pa)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(220, 289);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 17);
            this.label9.TabIndex = 55;
            this.label9.Text = "FR Specific Gravity";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(204, 261);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 17);
            this.label8.TabIndex = 54;
            this.label8.Text = "FR Viscosity (Pa.Sec)";
            // 
            // isenExp_txtBox
            // 
            this.isenExp_txtBox.Location = new System.Drawing.Point(354, 228);
            this.isenExp_txtBox.Name = "isenExp_txtBox";
            this.isenExp_txtBox.Size = new System.Drawing.Size(64, 22);
            this.isenExp_txtBox.TabIndex = 53;
            // 
            // tapDistDown_txtBox
            // 
            this.tapDistDown_txtBox.Location = new System.Drawing.Point(354, 200);
            this.tapDistDown_txtBox.Name = "tapDistDown_txtBox";
            this.tapDistDown_txtBox.Size = new System.Drawing.Size(64, 22);
            this.tapDistDown_txtBox.TabIndex = 52;
            // 
            // tapDistUp_txtBox
            // 
            this.tapDistUp_txtBox.Location = new System.Drawing.Point(354, 172);
            this.tapDistUp_txtBox.Name = "tapDistUp_txtBox";
            this.tapDistUp_txtBox.Size = new System.Drawing.Size(64, 22);
            this.tapDistUp_txtBox.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(174, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 17);
            this.label7.TabIndex = 50;
            this.label7.Text = "FR Isentropic Exponent - k";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(320, 17);
            this.label6.TabIndex = 49;
            this.label6.Text = "FR Tapping Distance Pressure Downstream (mm)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(303, 17);
            this.label5.TabIndex = 48;
            this.label5.Text = "FR Tapping Distance Pressure Upstream (mm)";
            // 
            // plateSize_txtBox
            // 
            this.plateSize_txtBox.Location = new System.Drawing.Point(354, 144);
            this.plateSize_txtBox.Name = "plateSize_txtBox";
            this.plateSize_txtBox.Size = new System.Drawing.Size(64, 22);
            this.plateSize_txtBox.TabIndex = 46;
            // 
            // pipeSize_txtBox
            // 
            this.pipeSize_txtBox.Location = new System.Drawing.Point(354, 116);
            this.pipeSize_txtBox.Name = "pipeSize_txtBox";
            this.pipeSize_txtBox.Size = new System.Drawing.Size(64, 22);
            this.pipeSize_txtBox.TabIndex = 45;
            // 
            // failVal_txtBox
            // 
            this.failVal_txtBox.Location = new System.Drawing.Point(354, 88);
            this.failVal_txtBox.Name = "failVal_txtBox";
            this.failVal_txtBox.Size = new System.Drawing.Size(64, 22);
            this.failVal_txtBox.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 17);
            this.label4.TabIndex = 42;
            this.label4.Text = "FR Plate Size (mm)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(223, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 17);
            this.label2.TabIndex = 41;
            this.label2.Text = "FR Pipe Size (mm)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 17);
            this.label1.TabIndex = 40;
            this.label1.Text = "FR Fail Value (kg/s)";
            // 
            // unitOffset_txtBox
            // 
            this.unitOffset_txtBox.Location = new System.Drawing.Point(354, 424);
            this.unitOffset_txtBox.Name = "unitOffset_txtBox";
            this.unitOffset_txtBox.Size = new System.Drawing.Size(64, 22);
            this.unitOffset_txtBox.TabIndex = 80;
            // 
            // unitScale_txtBox
            // 
            this.unitScale_txtBox.Location = new System.Drawing.Point(354, 396);
            this.unitScale_txtBox.Name = "unitScale_txtBox";
            this.unitScale_txtBox.Size = new System.Drawing.Size(64, 22);
            this.unitScale_txtBox.TabIndex = 79;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(251, 429);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 78;
            this.label3.Text = "FR Unit Offset";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(254, 401);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 17);
            this.label19.TabIndex = 77;
            this.label19.Text = "FR Unit Scale";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(214, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(205, 20);
            this.label20.TabIndex = 81;
            this.label20.Text = "Flow Run Configuration";
            // 
            // setDef_comBox
            // 
            this.setDef_comBox.Location = new System.Drawing.Point(817, 403);
            this.setDef_comBox.Name = "setDef_comBox";
            this.setDef_comBox.Size = new System.Drawing.Size(97, 43);
            this.setDef_comBox.TabIndex = 82;
            this.setDef_comBox.Text = "Set Default";
            this.setDef_comBox.UseVisualStyleBackColor = true;
            this.setDef_comBox.Click += new System.EventHandler(this.setDef_comBox_Click);
            // 
            // iso5167calc_radiobtn
            // 
            this.iso5167calc_radiobtn.AutoSize = true;
            this.iso5167calc_radiobtn.Checked = true;
            this.iso5167calc_radiobtn.Location = new System.Drawing.Point(685, 290);
            this.iso5167calc_radiobtn.Name = "iso5167calc_radiobtn";
            this.iso5167calc_radiobtn.Size = new System.Drawing.Size(209, 21);
            this.iso5167calc_radiobtn.TabIndex = 94;
            this.iso5167calc_radiobtn.TabStop = true;
            this.iso5167calc_radiobtn.Text = "ISO 5167 : 2003 Orifice Flow";
            this.iso5167calc_radiobtn.UseVisualStyleBackColor = true;
            // 
            // noWetCor_radiobtn
            // 
            this.noWetCor_radiobtn.AutoSize = true;
            this.noWetCor_radiobtn.Checked = true;
            this.noWetCor_radiobtn.Location = new System.Drawing.Point(685, 263);
            this.noWetCor_radiobtn.Name = "noWetCor_radiobtn";
            this.noWetCor_radiobtn.Size = new System.Drawing.Size(145, 21);
            this.noWetCor_radiobtn.TabIndex = 95;
            this.noWetCor_radiobtn.TabStop = true;
            this.noWetCor_radiobtn.Text = "No Wet Correction";
            this.noWetCor_radiobtn.UseVisualStyleBackColor = true;
            // 
            // FluidPropCalc_comBox
            // 
            this.FluidPropCalc_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FluidPropCalc_comBox.FormattingEnabled = true;
            this.FluidPropCalc_comBox.Items.AddRange(new object[] {
            "AGA-8(1994)Detail Method",
            "AGA-8(1994)GERG008 Method",
            "Fluid Library"});
            this.FluidPropCalc_comBox.Location = new System.Drawing.Point(685, 202);
            this.FluidPropCalc_comBox.Name = "FluidPropCalc_comBox";
            this.FluidPropCalc_comBox.Size = new System.Drawing.Size(229, 24);
            this.FluidPropCalc_comBox.TabIndex = 96;
            this.FluidPropCalc_comBox.SelectedIndexChanged += new System.EventHandler(this.FluidPropCalc_comBox_SelectedIndexChanged);
            // 
            // fluidName_comBox
            // 
            this.fluidName_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fluidName_comBox.Enabled = false;
            this.fluidName_comBox.FormattingEnabled = true;
            this.fluidName_comBox.Items.AddRange(new object[] {
            "1_Butene",
            "Acetone",
            "Air",
            "Ammonia",
            "Argon",
            "Benzene",
            "CarbonDioxide",
            "CarbonMonoxide",
            "CarbonylSulfide",
            "CycloHexane",
            "CycloPropane",
            "Cyclopentane",
            "D4",
            "D5",
            "D6",
            "Deuterium",
            "Dichloroethane",
            "DiethylEther",
            "DimethylCarbonate",
            "DimethylEther",
            "Ethane",
            "Ethanol",
            "EthylBenzene",
            "Ethylene",
            "EthyleneOxide",
            "Fluorine",
            "HFE143m",
            "HeavyWater",
            "Helium",
            "Hydrogen",
            "HydrogenChloride",
            "HydrogenSulfide",
            "IsoButane",
            "IsoButene",
            "Isohexane",
            "Isopentane",
            "Krypton",
            "MD2M",
            "MD3M",
            "MD4M",
            "MDM",
            "MM",
            "Methane",
            "Methanol",
            "MethylLinoleate",
            "MethylLinolenate",
            "MethylOleate",
            "MethylPalmitate",
            "MethylStearate",
            "Neon",
            "Neopentane",
            "Nitrogen",
            "NitrousOxide",
            "Novec649",
            "OrthoDeuterium",
            "OrthoHydrogen,",
            "Oxygen",
            "ParaDeuterium",
            "ParaHydrogen",
            "Propylene",
            "Propyne",
            "R11",
            "R113",
            "R114",
            "R115",
            "R116",
            "R12",
            "R123",
            "R1233zd_E",
            "R1234yf",
            "R1234ze_E",
            "R1234ze_Z",
            "R124",
            "R1243zf",
            "R125",
            "R13",
            "R134a",
            "R13I1",
            "R14",
            "R141b",
            "R142b",
            "R143a",
            "R152A",
            "R161",
            "R21",
            "R218",
            "R22",
            "R227EA",
            "R23",
            "R236EA",
            "R236FA",
            "R245ca",
            "R245fa",
            "R32",
            "R365MFC",
            "R40",
            "R404A",
            "R407C",
            "R41",
            "R410A",
            "R507A",
            "RC318",
            "SES36",
            "SulfurDioxide",
            "SulfurHexafluoride",
            "Toluene",
            "Water",
            "Xenon",
            "cis_two_Butene",
            "m_Xylene",
            "n_Butane",
            "n_Decane",
            "n_Dodecane",
            "n_Heptane",
            "n_Hexane",
            "n_Nonane",
            "n_Octane",
            "n_Pentane",
            "n_Propane",
            "n_Undecane",
            "o_Xylene",
            "p_Xylene",
            "trans_2_Butene"});
            this.fluidName_comBox.Location = new System.Drawing.Point(685, 233);
            this.fluidName_comBox.Name = "fluidName_comBox";
            this.fluidName_comBox.Size = new System.Drawing.Size(145, 24);
            this.fluidName_comBox.TabIndex = 98;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(597, 239);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 17);
            this.label21.TabIndex = 97;
            this.label21.Text = "Fluid Name";
            // 
            // ethernetIPforCLXCom1
            // 
            this.ethernetIPforCLXCom1.CIPConnectionSize = 508;
            this.ethernetIPforCLXCom1.DisableMultiServiceRequest = false;
            this.ethernetIPforCLXCom1.DisableSubscriptions = false;
            this.ethernetIPforCLXCom1.IniFileName = "";
            this.ethernetIPforCLXCom1.IniFileSection = null;
            this.ethernetIPforCLXCom1.IPAddress = "192.168.0.10";
            this.ethernetIPforCLXCom1.PollRateOverride = 500;
            this.ethernetIPforCLXCom1.Port = 44818;
            this.ethernetIPforCLXCom1.ProcessorSlot = 0;
            this.ethernetIPforCLXCom1.RoutePath = null;
            this.ethernetIPforCLXCom1.Timeout = 4000;
            // 
            // veloSelect_comBox
            // 
            this.veloSelect_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.veloSelect_comBox.FormattingEnabled = true;
            this.veloSelect_comBox.Items.AddRange(new object[] {
            "Manual Entry",
            "Calculated"});
            this.veloSelect_comBox.Location = new System.Drawing.Point(685, 83);
            this.veloSelect_comBox.Name = "veloSelect_comBox";
            this.veloSelect_comBox.Size = new System.Drawing.Size(120, 24);
            this.veloSelect_comBox.TabIndex = 99;
            // 
            // densitySelect_comBox
            // 
            this.densitySelect_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.densitySelect_comBox.FormattingEnabled = true;
            this.densitySelect_comBox.Items.AddRange(new object[] {
            "Manual Entry",
            "Calculated"});
            this.densitySelect_comBox.Location = new System.Drawing.Point(685, 113);
            this.densitySelect_comBox.Name = "densitySelect_comBox";
            this.densitySelect_comBox.Size = new System.Drawing.Size(120, 24);
            this.densitySelect_comBox.TabIndex = 100;
            // 
            // viscoSelect_comBox
            // 
            this.viscoSelect_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.viscoSelect_comBox.FormattingEnabled = true;
            this.viscoSelect_comBox.Items.AddRange(new object[] {
            "Manual Entry",
            "Calculated"});
            this.viscoSelect_comBox.Location = new System.Drawing.Point(685, 143);
            this.viscoSelect_comBox.Name = "viscoSelect_comBox";
            this.viscoSelect_comBox.Size = new System.Drawing.Size(120, 24);
            this.viscoSelect_comBox.TabIndex = 101;
            // 
            // isentropicSelect_comBox
            // 
            this.isentropicSelect_comBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.isentropicSelect_comBox.FormattingEnabled = true;
            this.isentropicSelect_comBox.Items.AddRange(new object[] {
            "Manual Entry",
            "Calculated"});
            this.isentropicSelect_comBox.Location = new System.Drawing.Point(685, 172);
            this.isentropicSelect_comBox.Name = "isentropicSelect_comBox";
            this.isentropicSelect_comBox.Size = new System.Drawing.Size(120, 24);
            this.isentropicSelect_comBox.TabIndex = 102;
            // 
            // tagName_txtBox
            // 
            this.tagName_txtBox.Location = new System.Drawing.Point(354, 12);
            this.tagName_txtBox.Name = "tagName_txtBox";
            this.tagName_txtBox.Size = new System.Drawing.Size(322, 22);
            this.tagName_txtBox.TabIndex = 103;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(274, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 17);
            this.label22.TabIndex = 104;
            this.label22.Text = "Tag Name";
            // 
            // isoCalcCfg_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 482);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.tagName_txtBox);
            this.Controls.Add(this.isentropicSelect_comBox);
            this.Controls.Add(this.viscoSelect_comBox);
            this.Controls.Add(this.densitySelect_comBox);
            this.Controls.Add(this.veloSelect_comBox);
            this.Controls.Add(this.fluidName_comBox);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.FluidPropCalc_comBox);
            this.Controls.Add(this.noWetCor_radiobtn);
            this.Controls.Add(this.iso5167calc_radiobtn);
            this.Controls.Add(this.setDef_comBox);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.unitOffset_txtBox);
            this.Controls.Add(this.unitScale_txtBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.dens_txtBox);
            this.Controls.Add(this.velo_txtBox);
            this.Controls.Add(this.atmPress_txtBox);
            this.Controls.Add(this.specGrav_txtBox);
            this.Controls.Add(this.visco_txtBox);
            this.Controls.Add(this.readKS_MVCfg_btn);
            this.Controls.Add(this.saveKS_MVCfg_btn);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.isenExp_txtBox);
            this.Controls.Add(this.tapDistDown_txtBox);
            this.Controls.Add(this.tapDistUp_txtBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.plateSize_txtBox);
            this.Controls.Add(this.pipeSize_txtBox);
            this.Controls.Add(this.failVal_txtBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "isoCalcCfg_form";
            this.Text = "Device Calculation Configuration";
            this.Load += new System.EventHandler(this.isoCalcCfg_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ethernetIPforCLXCom1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox dens_txtBox;
        private System.Windows.Forms.TextBox velo_txtBox;
        private System.Windows.Forms.TextBox atmPress_txtBox;
        private System.Windows.Forms.TextBox specGrav_txtBox;
        private System.Windows.Forms.TextBox visco_txtBox;
        private System.Windows.Forms.Button readKS_MVCfg_btn;
        private System.Windows.Forms.Button saveKS_MVCfg_btn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox isenExp_txtBox;
        private System.Windows.Forms.TextBox tapDistDown_txtBox;
        private System.Windows.Forms.TextBox tapDistUp_txtBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox plateSize_txtBox;
        private System.Windows.Forms.TextBox pipeSize_txtBox;
        private System.Windows.Forms.TextBox failVal_txtBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox unitOffset_txtBox;
        private System.Windows.Forms.TextBox unitScale_txtBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button setDef_comBox;
        private System.Windows.Forms.RadioButton iso5167calc_radiobtn;
        private System.Windows.Forms.RadioButton noWetCor_radiobtn;
        private System.Windows.Forms.ComboBox FluidPropCalc_comBox;
        private System.Windows.Forms.ComboBox fluidName_comBox;
        private System.Windows.Forms.Label label21;
        private AdvancedHMIDrivers.EthernetIPforCLXCom ethernetIPforCLXCom1;
        private System.Windows.Forms.ComboBox veloSelect_comBox;
        private System.Windows.Forms.ComboBox densitySelect_comBox;
        private System.Windows.Forms.ComboBox viscoSelect_comBox;
        private System.Windows.Forms.ComboBox isentropicSelect_comBox;
        private System.Windows.Forms.TextBox tagName_txtBox;
        private System.Windows.Forms.Label label22;
    }
}