﻿
namespace KS_MV_SoftwareModbus
{
    partial class liveData_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.exitLiveData_btn = new System.Windows.Forms.Button();
            this.runLiveData_btn = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.compress_txtBox = new System.Windows.Forms.TextBox();
            this.dens_txtBox = new System.Windows.Forms.TextBox();
            this.isentro_txtBox = new System.Windows.Forms.TextBox();
            this.visco_txtBox = new System.Windows.Forms.TextBox();
            this.diffPress_txtBox = new System.Windows.Forms.TextBox();
            this.reynold_txtBox = new System.Windows.Forms.TextBox();
            this.pressLossCoef_txtBox = new System.Windows.Forms.TextBox();
            this.pressLoss_txtBox = new System.Windows.Forms.TextBox();
            this.specGrav_txtBox = new System.Windows.Forms.TextBox();
            this.downPress_txtBox = new System.Windows.Forms.TextBox();
            this.upPress_txtBox = new System.Windows.Forms.TextBox();
            this.procTemp_txtBox = new System.Windows.Forms.TextBox();
            this.logKe_txtBox = new System.Windows.Forms.TextBox();
            this.volumeFlow_txtBox = new System.Windows.Forms.TextBox();
            this.massFlow_txtBox = new System.Windows.Forms.TextBox();
            this.flowExt_txtBox = new System.Windows.Forms.TextBox();
            this.expFact_txtBox = new System.Windows.Forms.TextBox();
            this.cd_txtBox = new System.Windows.Forms.TextBox();
            this.jtCoef_txtBox = new System.Windows.Forms.TextBox();
            this.beta_txtBox = new System.Windows.Forms.TextBox();
            this.plateSize_txtBox = new System.Windows.Forms.TextBox();
            this.pipeSize_txtBox = new System.Windows.Forms.TextBox();
            this.velo_txtBox = new System.Windows.Forms.TextBox();
            this.timeStamp_txtBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tagNameLiveDat_txtBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.fluidPropCalc_txtBox = new System.Windows.Forms.TextBox();
            this.modeCalc_txtBox = new System.Windows.Forms.TextBox();
            this.wetCor_txtBox = new System.Windows.Forms.TextBox();
            this.fluidName_comBox = new System.Windows.Forms.ComboBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.timeNowDevice_txtBox = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Difference Pressure";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 365);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Temperature";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Static Pressure";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Device";
            // 
            // textBox5
            // 
            this.textBox5.Cursor = System.Windows.Forms.Cursors.No;
            this.textBox5.Location = new System.Drawing.Point(21, 202);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(119, 22);
            this.textBox5.TabIndex = 11;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Calculation";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(252, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Compressibility";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(247, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Density (kg/m3)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(222, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Isentropic Exponent";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(232, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "Viscosity (Pa.Sec)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(240, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "Reynold Number";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(168, 319);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(186, 17);
            this.label11.TabIndex = 17;
            this.label11.Text = "Differensial Pressure (Pa.G)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(186, 347);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Process Temperature (K)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(563, 319);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 17);
            this.label13.TabIndex = 22;
            this.label13.Text = "Coefisien Discharge";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(590, 291);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 17);
            this.label14.TabIndex = 21;
            this.label14.Text = "JT Coef (K/kPa)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(585, 263);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 17);
            this.label15.TabIndex = 20;
            this.label15.Text = "Beta (Corrected)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(515, 235);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(183, 17);
            this.label16.TabIndex = 19;
            this.label16.Text = "Plate Size (Corrected) (mm)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(224, 459);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 17);
            this.label17.TabIndex = 26;
            this.label17.Text = "Pressure Loss (Pa)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(247, 430);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 17);
            this.label18.TabIndex = 25;
            this.label18.Text = "Specific Gravity";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(160, 403);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(193, 17);
            this.label19.TabIndex = 24;
            this.label19.Text = "Downstream Pressure (Pa G)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(177, 375);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(176, 17);
            this.label20.TabIndex = 23;
            this.label20.Text = "Upstream Pressure (Pa G)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(518, 207);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(179, 17);
            this.label21.TabIndex = 30;
            this.label21.Text = "Pipe Size (Corrected) (mm)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(524, 179);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(174, 17);
            this.label23.TabIndex = 28;
            this.label23.Text = "Velocity of Upstream (m/s)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(192, 484);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(162, 17);
            this.label24.TabIndex = 27;
            this.label24.Text = "Pressure loss coefficient";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(551, 403);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(147, 17);
            this.label25.TabIndex = 34;
            this.label25.Text = "Mass Flow Rate (kg/s)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(597, 375);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(101, 17);
            this.label27.TabIndex = 32;
            this.label27.Text = "Flow Extension";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(581, 347);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(117, 17);
            this.label28.TabIndex = 31;
            this.label28.Text = "Expansion Factor";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(526, 430);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(172, 17);
            this.label29.TabIndex = 35;
            this.label29.Text = "Volume Flow Rate (m^3/s)";
            // 
            // exitLiveData_btn
            // 
            this.exitLiveData_btn.Location = new System.Drawing.Point(816, 573);
            this.exitLiveData_btn.Name = "exitLiveData_btn";
            this.exitLiveData_btn.Size = new System.Drawing.Size(105, 41);
            this.exitLiveData_btn.TabIndex = 59;
            this.exitLiveData_btn.Text = "Exit";
            this.exitLiveData_btn.UseVisualStyleBackColor = true;
            this.exitLiveData_btn.Click += new System.EventHandler(this.exitLiveData_btn_Click);
            // 
            // runLiveData_btn
            // 
            this.runLiveData_btn.Location = new System.Drawing.Point(705, 573);
            this.runLiveData_btn.Name = "runLiveData_btn";
            this.runLiveData_btn.Size = new System.Drawing.Size(105, 41);
            this.runLiveData_btn.TabIndex = 60;
            this.runLiveData_btn.Text = "Run";
            this.runLiveData_btn.UseVisualStyleBackColor = true;
            this.runLiveData_btn.Click += new System.EventHandler(this.runLiveData_btn_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(618, 459);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 17);
            this.label22.TabIndex = 61;
            this.label22.Text = "Logging Ke";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(614, 487);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 17);
            this.label30.TabIndex = 63;
            this.label30.Text = "Time Stamp";
            // 
            // textBox4
            // 
            this.textBox4.Cursor = System.Windows.Forms.Cursors.No;
            this.textBox4.Location = new System.Drawing.Point(21, 247);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(119, 22);
            this.textBox4.TabIndex = 66;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Cursor = System.Windows.Forms.Cursors.No;
            this.textBox1.Location = new System.Drawing.Point(21, 292);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(119, 22);
            this.textBox1.TabIndex = 67;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Cursor = System.Windows.Forms.Cursors.No;
            this.textBox2.Location = new System.Drawing.Point(21, 342);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(119, 22);
            this.textBox2.TabIndex = 68;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // compress_txtBox
            // 
            this.compress_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.compress_txtBox.Location = new System.Drawing.Point(360, 176);
            this.compress_txtBox.Name = "compress_txtBox";
            this.compress_txtBox.ReadOnly = true;
            this.compress_txtBox.Size = new System.Drawing.Size(119, 22);
            this.compress_txtBox.TabIndex = 69;
            this.compress_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dens_txtBox
            // 
            this.dens_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.dens_txtBox.Location = new System.Drawing.Point(360, 204);
            this.dens_txtBox.Name = "dens_txtBox";
            this.dens_txtBox.ReadOnly = true;
            this.dens_txtBox.Size = new System.Drawing.Size(119, 22);
            this.dens_txtBox.TabIndex = 70;
            this.dens_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // isentro_txtBox
            // 
            this.isentro_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.isentro_txtBox.Location = new System.Drawing.Point(360, 260);
            this.isentro_txtBox.Name = "isentro_txtBox";
            this.isentro_txtBox.ReadOnly = true;
            this.isentro_txtBox.Size = new System.Drawing.Size(119, 22);
            this.isentro_txtBox.TabIndex = 72;
            this.isentro_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // visco_txtBox
            // 
            this.visco_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.visco_txtBox.Location = new System.Drawing.Point(360, 232);
            this.visco_txtBox.Name = "visco_txtBox";
            this.visco_txtBox.ReadOnly = true;
            this.visco_txtBox.Size = new System.Drawing.Size(119, 22);
            this.visco_txtBox.TabIndex = 71;
            this.visco_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // diffPress_txtBox
            // 
            this.diffPress_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.diffPress_txtBox.Location = new System.Drawing.Point(360, 316);
            this.diffPress_txtBox.Name = "diffPress_txtBox";
            this.diffPress_txtBox.ReadOnly = true;
            this.diffPress_txtBox.Size = new System.Drawing.Size(119, 22);
            this.diffPress_txtBox.TabIndex = 74;
            this.diffPress_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // reynold_txtBox
            // 
            this.reynold_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.reynold_txtBox.Location = new System.Drawing.Point(360, 288);
            this.reynold_txtBox.Name = "reynold_txtBox";
            this.reynold_txtBox.ReadOnly = true;
            this.reynold_txtBox.Size = new System.Drawing.Size(119, 22);
            this.reynold_txtBox.TabIndex = 73;
            this.reynold_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pressLossCoef_txtBox
            // 
            this.pressLossCoef_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.pressLossCoef_txtBox.Location = new System.Drawing.Point(360, 484);
            this.pressLossCoef_txtBox.Name = "pressLossCoef_txtBox";
            this.pressLossCoef_txtBox.ReadOnly = true;
            this.pressLossCoef_txtBox.Size = new System.Drawing.Size(119, 22);
            this.pressLossCoef_txtBox.TabIndex = 80;
            this.pressLossCoef_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pressLoss_txtBox
            // 
            this.pressLoss_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.pressLoss_txtBox.Location = new System.Drawing.Point(360, 456);
            this.pressLoss_txtBox.Name = "pressLoss_txtBox";
            this.pressLoss_txtBox.ReadOnly = true;
            this.pressLoss_txtBox.Size = new System.Drawing.Size(119, 22);
            this.pressLoss_txtBox.TabIndex = 79;
            this.pressLoss_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // specGrav_txtBox
            // 
            this.specGrav_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.specGrav_txtBox.Location = new System.Drawing.Point(360, 428);
            this.specGrav_txtBox.Name = "specGrav_txtBox";
            this.specGrav_txtBox.ReadOnly = true;
            this.specGrav_txtBox.Size = new System.Drawing.Size(119, 22);
            this.specGrav_txtBox.TabIndex = 78;
            this.specGrav_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // downPress_txtBox
            // 
            this.downPress_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.downPress_txtBox.Location = new System.Drawing.Point(360, 400);
            this.downPress_txtBox.Name = "downPress_txtBox";
            this.downPress_txtBox.ReadOnly = true;
            this.downPress_txtBox.Size = new System.Drawing.Size(119, 22);
            this.downPress_txtBox.TabIndex = 77;
            this.downPress_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // upPress_txtBox
            // 
            this.upPress_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.upPress_txtBox.Location = new System.Drawing.Point(360, 372);
            this.upPress_txtBox.Name = "upPress_txtBox";
            this.upPress_txtBox.ReadOnly = true;
            this.upPress_txtBox.Size = new System.Drawing.Size(119, 22);
            this.upPress_txtBox.TabIndex = 76;
            this.upPress_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // procTemp_txtBox
            // 
            this.procTemp_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.procTemp_txtBox.Location = new System.Drawing.Point(360, 344);
            this.procTemp_txtBox.Name = "procTemp_txtBox";
            this.procTemp_txtBox.ReadOnly = true;
            this.procTemp_txtBox.Size = new System.Drawing.Size(119, 22);
            this.procTemp_txtBox.TabIndex = 75;
            this.procTemp_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // logKe_txtBox
            // 
            this.logKe_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.logKe_txtBox.Location = new System.Drawing.Point(704, 456);
            this.logKe_txtBox.Name = "logKe_txtBox";
            this.logKe_txtBox.ReadOnly = true;
            this.logKe_txtBox.Size = new System.Drawing.Size(119, 22);
            this.logKe_txtBox.TabIndex = 92;
            this.logKe_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // volumeFlow_txtBox
            // 
            this.volumeFlow_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.volumeFlow_txtBox.Location = new System.Drawing.Point(704, 428);
            this.volumeFlow_txtBox.Name = "volumeFlow_txtBox";
            this.volumeFlow_txtBox.ReadOnly = true;
            this.volumeFlow_txtBox.Size = new System.Drawing.Size(119, 22);
            this.volumeFlow_txtBox.TabIndex = 91;
            this.volumeFlow_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // massFlow_txtBox
            // 
            this.massFlow_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.massFlow_txtBox.Location = new System.Drawing.Point(704, 400);
            this.massFlow_txtBox.Name = "massFlow_txtBox";
            this.massFlow_txtBox.ReadOnly = true;
            this.massFlow_txtBox.Size = new System.Drawing.Size(119, 22);
            this.massFlow_txtBox.TabIndex = 90;
            this.massFlow_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowExt_txtBox
            // 
            this.flowExt_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.flowExt_txtBox.Location = new System.Drawing.Point(704, 372);
            this.flowExt_txtBox.Name = "flowExt_txtBox";
            this.flowExt_txtBox.ReadOnly = true;
            this.flowExt_txtBox.Size = new System.Drawing.Size(119, 22);
            this.flowExt_txtBox.TabIndex = 88;
            this.flowExt_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // expFact_txtBox
            // 
            this.expFact_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.expFact_txtBox.Location = new System.Drawing.Point(704, 344);
            this.expFact_txtBox.Name = "expFact_txtBox";
            this.expFact_txtBox.ReadOnly = true;
            this.expFact_txtBox.Size = new System.Drawing.Size(119, 22);
            this.expFact_txtBox.TabIndex = 87;
            this.expFact_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cd_txtBox
            // 
            this.cd_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.cd_txtBox.Location = new System.Drawing.Point(704, 316);
            this.cd_txtBox.Name = "cd_txtBox";
            this.cd_txtBox.ReadOnly = true;
            this.cd_txtBox.Size = new System.Drawing.Size(119, 22);
            this.cd_txtBox.TabIndex = 86;
            this.cd_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // jtCoef_txtBox
            // 
            this.jtCoef_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.jtCoef_txtBox.Location = new System.Drawing.Point(704, 288);
            this.jtCoef_txtBox.Name = "jtCoef_txtBox";
            this.jtCoef_txtBox.ReadOnly = true;
            this.jtCoef_txtBox.Size = new System.Drawing.Size(119, 22);
            this.jtCoef_txtBox.TabIndex = 85;
            this.jtCoef_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // beta_txtBox
            // 
            this.beta_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.beta_txtBox.Location = new System.Drawing.Point(704, 260);
            this.beta_txtBox.Name = "beta_txtBox";
            this.beta_txtBox.ReadOnly = true;
            this.beta_txtBox.Size = new System.Drawing.Size(119, 22);
            this.beta_txtBox.TabIndex = 84;
            this.beta_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // plateSize_txtBox
            // 
            this.plateSize_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.plateSize_txtBox.Location = new System.Drawing.Point(704, 232);
            this.plateSize_txtBox.Name = "plateSize_txtBox";
            this.plateSize_txtBox.ReadOnly = true;
            this.plateSize_txtBox.Size = new System.Drawing.Size(119, 22);
            this.plateSize_txtBox.TabIndex = 83;
            this.plateSize_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pipeSize_txtBox
            // 
            this.pipeSize_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.pipeSize_txtBox.Location = new System.Drawing.Point(704, 204);
            this.pipeSize_txtBox.Name = "pipeSize_txtBox";
            this.pipeSize_txtBox.ReadOnly = true;
            this.pipeSize_txtBox.Size = new System.Drawing.Size(119, 22);
            this.pipeSize_txtBox.TabIndex = 82;
            this.pipeSize_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // velo_txtBox
            // 
            this.velo_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.velo_txtBox.Location = new System.Drawing.Point(704, 176);
            this.velo_txtBox.Name = "velo_txtBox";
            this.velo_txtBox.ReadOnly = true;
            this.velo_txtBox.Size = new System.Drawing.Size(119, 22);
            this.velo_txtBox.TabIndex = 81;
            this.velo_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timeStamp_txtBox
            // 
            this.timeStamp_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.timeStamp_txtBox.Location = new System.Drawing.Point(704, 484);
            this.timeStamp_txtBox.Name = "timeStamp_txtBox";
            this.timeStamp_txtBox.ReadOnly = true;
            this.timeStamp_txtBox.Size = new System.Drawing.Size(217, 22);
            this.timeStamp_txtBox.TabIndex = 93;
            this.timeStamp_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(281, 15);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(74, 17);
            this.label31.TabIndex = 106;
            this.label31.Text = "Tag Name";
            // 
            // tagNameLiveDat_txtBox
            // 
            this.tagNameLiveDat_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.tagNameLiveDat_txtBox.Location = new System.Drawing.Point(361, 12);
            this.tagNameLiveDat_txtBox.Name = "tagNameLiveDat_txtBox";
            this.tagNameLiveDat_txtBox.ReadOnly = true;
            this.tagNameLiveDat_txtBox.Size = new System.Drawing.Size(322, 22);
            this.tagNameLiveDat_txtBox.TabIndex = 107;
            this.tagNameLiveDat_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(276, 96);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(79, 17);
            this.label32.TabIndex = 112;
            this.label32.Text = "Fluid Name";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(186, 68);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(169, 17);
            this.label33.TabIndex = 108;
            this.label33.Text = "Fluid Property Calculation";
            // 
            // fluidPropCalc_txtBox
            // 
            this.fluidPropCalc_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.fluidPropCalc_txtBox.Location = new System.Drawing.Point(361, 65);
            this.fluidPropCalc_txtBox.Name = "fluidPropCalc_txtBox";
            this.fluidPropCalc_txtBox.ReadOnly = true;
            this.fluidPropCalc_txtBox.Size = new System.Drawing.Size(229, 22);
            this.fluidPropCalc_txtBox.TabIndex = 114;
            this.fluidPropCalc_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // modeCalc_txtBox
            // 
            this.modeCalc_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.modeCalc_txtBox.Location = new System.Drawing.Point(621, 93);
            this.modeCalc_txtBox.Name = "modeCalc_txtBox";
            this.modeCalc_txtBox.ReadOnly = true;
            this.modeCalc_txtBox.Size = new System.Drawing.Size(202, 22);
            this.modeCalc_txtBox.TabIndex = 116;
            this.modeCalc_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // wetCor_txtBox
            // 
            this.wetCor_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.wetCor_txtBox.Location = new System.Drawing.Point(621, 65);
            this.wetCor_txtBox.Name = "wetCor_txtBox";
            this.wetCor_txtBox.ReadOnly = true;
            this.wetCor_txtBox.Size = new System.Drawing.Size(202, 22);
            this.wetCor_txtBox.TabIndex = 117;
            this.wetCor_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fluidName_comBox
            // 
            this.fluidName_comBox.Enabled = false;
            this.fluidName_comBox.FormattingEnabled = true;
            this.fluidName_comBox.Items.AddRange(new object[] {
            "1_Butene",
            "Acetone",
            "Air",
            "Ammonia",
            "Argon",
            "Benzene",
            "CarbonDioxide",
            "CarbonMonoxide",
            "CarbonylSulfide",
            "CycloHexane",
            "CycloPropane",
            "Cyclopentane",
            "D4",
            "D5",
            "D6",
            "Deuterium",
            "Dichloroethane",
            "DiethylEther",
            "DimethylCarbonate",
            "DimethylEther",
            "Ethane",
            "Ethanol",
            "EthylBenzene",
            "Ethylene",
            "EthyleneOxide",
            "Fluorine",
            "HFE143m",
            "HeavyWater",
            "Helium",
            "Hydrogen",
            "HydrogenChloride",
            "HydrogenSulfide",
            "IsoButane",
            "IsoButene",
            "Isohexane",
            "Isopentane",
            "Krypton",
            "MD2M",
            "MD3M",
            "MD4M",
            "MDM",
            "MM",
            "Methane",
            "Methanol",
            "MethylLinoleate",
            "MethylLinolenate",
            "MethylOleate",
            "MethylPalmitate",
            "MethylStearate",
            "Neon",
            "Neopentane",
            "Nitrogen",
            "NitrousOxide",
            "Novec649",
            "OrthoDeuterium",
            "OrthoHydrogen,",
            "Oxygen",
            "ParaDeuterium",
            "ParaHydrogen",
            "Propylene",
            "Propyne",
            "R11",
            "R113",
            "R114",
            "R115",
            "R116",
            "R12",
            "R123",
            "R1233zd_E",
            "R1234yf",
            "R1234ze_E",
            "R1234ze_Z",
            "R124",
            "R1243zf",
            "R125",
            "R13",
            "R134a",
            "R13I1",
            "R14",
            "R141b",
            "R142b",
            "R143a",
            "R152A",
            "R161",
            "R21",
            "R218",
            "R22",
            "R227EA",
            "R23",
            "R236EA",
            "R236FA",
            "R245ca",
            "R245fa",
            "R32",
            "R365MFC",
            "R40",
            "R404A",
            "R407C",
            "R41",
            "R410A",
            "R507A",
            "RC318",
            "SES36",
            "SulfurDioxide",
            "SulfurHexafluoride",
            "Toluene",
            "Water",
            "Xenon",
            "cis_two_Butene",
            "m_Xylene",
            "n_Butane",
            "n_Decane",
            "n_Dodecane",
            "n_Heptane",
            "n_Hexane",
            "n_Nonane",
            "n_Octane",
            "n_Pentane",
            "n_Propane",
            "n_Undecane",
            "o_Xylene",
            "p_Xylene",
            "trans_2_Butene"});
            this.fluidName_comBox.Location = new System.Drawing.Point(361, 93);
            this.fluidName_comBox.Name = "fluidName_comBox";
            this.fluidName_comBox.Size = new System.Drawing.Size(119, 24);
            this.fluidName_comBox.TabIndex = 118;
            // 
            // textBox3
            // 
            this.textBox3.Cursor = System.Windows.Forms.Cursors.No;
            this.textBox3.Location = new System.Drawing.Point(21, 385);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(119, 22);
            this.textBox3.TabIndex = 119;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(701, 509);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(182, 17);
            this.label26.TabIndex = 120;
            this.label26.Text = "(YYYY/MM/DD)(HH:MM:SS)";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(148, 588);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(182, 17);
            this.label34.TabIndex = 123;
            this.label34.Text = "(YYYY/MM/DD)(HH:MM:SS)";
            // 
            // timeNowDevice_txtBox
            // 
            this.timeNowDevice_txtBox.Cursor = System.Windows.Forms.Cursors.No;
            this.timeNowDevice_txtBox.Location = new System.Drawing.Point(151, 563);
            this.timeNowDevice_txtBox.Name = "timeNowDevice_txtBox";
            this.timeNowDevice_txtBox.ReadOnly = true;
            this.timeNowDevice_txtBox.Size = new System.Drawing.Size(217, 22);
            this.timeNowDevice_txtBox.TabIndex = 122;
            this.timeNowDevice_txtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(28, 566);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(117, 17);
            this.label35.TabIndex = 121;
            this.label35.Text = "Time Device Now";
            // 
            // liveData_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 626);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.timeNowDevice_txtBox);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.fluidName_comBox);
            this.Controls.Add(this.wetCor_txtBox);
            this.Controls.Add(this.modeCalc_txtBox);
            this.Controls.Add(this.fluidPropCalc_txtBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.tagNameLiveDat_txtBox);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.timeStamp_txtBox);
            this.Controls.Add(this.logKe_txtBox);
            this.Controls.Add(this.volumeFlow_txtBox);
            this.Controls.Add(this.massFlow_txtBox);
            this.Controls.Add(this.flowExt_txtBox);
            this.Controls.Add(this.expFact_txtBox);
            this.Controls.Add(this.cd_txtBox);
            this.Controls.Add(this.jtCoef_txtBox);
            this.Controls.Add(this.beta_txtBox);
            this.Controls.Add(this.plateSize_txtBox);
            this.Controls.Add(this.pipeSize_txtBox);
            this.Controls.Add(this.velo_txtBox);
            this.Controls.Add(this.pressLossCoef_txtBox);
            this.Controls.Add(this.pressLoss_txtBox);
            this.Controls.Add(this.specGrav_txtBox);
            this.Controls.Add(this.downPress_txtBox);
            this.Controls.Add(this.upPress_txtBox);
            this.Controls.Add(this.procTemp_txtBox);
            this.Controls.Add(this.diffPress_txtBox);
            this.Controls.Add(this.reynold_txtBox);
            this.Controls.Add(this.isentro_txtBox);
            this.Controls.Add(this.visco_txtBox);
            this.Controls.Add(this.dens_txtBox);
            this.Controls.Add(this.compress_txtBox);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.runLiveData_btn);
            this.Controls.Add(this.exitLiveData_btn);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "liveData_form";
            this.Text = "Live Data";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.liveData_formClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button exitLiveData_btn;
        private System.Windows.Forms.Button runLiveData_btn;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox compress_txtBox;
        private System.Windows.Forms.TextBox dens_txtBox;
        private System.Windows.Forms.TextBox isentro_txtBox;
        private System.Windows.Forms.TextBox visco_txtBox;
        private System.Windows.Forms.TextBox diffPress_txtBox;
        private System.Windows.Forms.TextBox reynold_txtBox;
        private System.Windows.Forms.TextBox pressLossCoef_txtBox;
        private System.Windows.Forms.TextBox pressLoss_txtBox;
        private System.Windows.Forms.TextBox specGrav_txtBox;
        private System.Windows.Forms.TextBox downPress_txtBox;
        private System.Windows.Forms.TextBox upPress_txtBox;
        private System.Windows.Forms.TextBox procTemp_txtBox;
        private System.Windows.Forms.TextBox logKe_txtBox;
        private System.Windows.Forms.TextBox volumeFlow_txtBox;
        private System.Windows.Forms.TextBox massFlow_txtBox;
        private System.Windows.Forms.TextBox flowExt_txtBox;
        private System.Windows.Forms.TextBox expFact_txtBox;
        private System.Windows.Forms.TextBox cd_txtBox;
        private System.Windows.Forms.TextBox jtCoef_txtBox;
        private System.Windows.Forms.TextBox beta_txtBox;
        private System.Windows.Forms.TextBox plateSize_txtBox;
        private System.Windows.Forms.TextBox pipeSize_txtBox;
        private System.Windows.Forms.TextBox velo_txtBox;
        private System.Windows.Forms.TextBox timeStamp_txtBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tagNameLiveDat_txtBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox fluidPropCalc_txtBox;
        private System.Windows.Forms.TextBox modeCalc_txtBox;
        private System.Windows.Forms.TextBox wetCor_txtBox;
        private System.Windows.Forms.ComboBox fluidName_comBox;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox timeNowDevice_txtBox;
        private System.Windows.Forms.Label label35;
    }
}