﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using CsvHelper;
using AdvancedHMIDrivers;


namespace KS_MV_SoftwareModbus
{

    public partial class comSetup_form : Form
    {
        public AdvancedHMIDrivers.ModbusRTUCom mBusCom_cfg;
        public comSetup_form(AdvancedHMIDrivers.ModbusRTUCom mbus)
        {
            InitializeComponent();
            mBusCom_cfg = mbus;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("\n\n======================");
            Console.WriteLine("Checking COM Parameter:");
            if (!string.IsNullOrEmpty(slvID_txtBox.Text) &&
                !string.IsNullOrEmpty(cboport.Text) &&
                !string.IsNullOrEmpty(baud_comBox.Text) &&
                !string.IsNullOrEmpty(dataBits_comBox.Text) &&
                !string.IsNullOrEmpty(parity_comBox.Text) &&
                !string.IsNullOrEmpty(stopBits_comBox.Text)
                )
            {

                Console.WriteLine("Slave ID: {0:D}", Int32.Parse(slvID_txtBox.Text));
                Console.WriteLine("Port: " + cboport.Text);
                Console.WriteLine("Baud Rate : " + Int32.Parse(baud_comBox.Text));
                Console.WriteLine("Data Bits: " + Int32.Parse(dataBits_comBox.Text));
                Console.WriteLine("Parity : " + parity_comBox.Text);
                Console.WriteLine("Stop Bits: " + stopBits_comBox.Text);
                Console.WriteLine("======================\n\n");

                mBusCom_cfg.BaudRate = Int32.Parse(baud_comBox.Text);
                mBusCom_cfg.DataBits = Int32.Parse(dataBits_comBox.Text);

                mBusCom_cfg.DisableSubscriptions = false;
                mBusCom_cfg.EnableLogging = false;
                mBusCom_cfg.IniFileName = "";
                mBusCom_cfg.IniFileSection = null;
                mBusCom_cfg.MaxReadGroupSize = 20;

                if (String.Equals("None", parity_comBox.Text))
                    mBusCom_cfg.Parity = System.IO.Ports.Parity.None;
                else if (String.Equals("Odd", parity_comBox.Text))
                    mBusCom_cfg.Parity = System.IO.Ports.Parity.Odd;
                else if (String.Equals("Even", parity_comBox.Text))
                    mBusCom_cfg.Parity = System.IO.Ports.Parity.Even;

                mBusCom_cfg.PollRateOverride = 500;
                mBusCom_cfg.PortName = cboport.Text;
                mBusCom_cfg.StationAddress = (byte)Int32.Parse(slvID_txtBox.Text);

                if (Int32.Parse(stopBits_comBox.Text) == 1)
                    mBusCom_cfg.StopBits = System.IO.Ports.StopBits.One;
                else if (Int32.Parse(stopBits_comBox.Text) == 2)
                    mBusCom_cfg.StopBits = System.IO.Ports.StopBits.Two;

                mBusCom_cfg.SwapBytes = true;
                mBusCom_cfg.SwapWords = true;
                mBusCom_cfg.TimeOut = 3000;

                MessageBox.Show("Open Port OK");
                this.Close();

            }
            else
            {
                MessageBox.Show("Error: Failed To Open Port!");
            }

        }

        private void comSetup_form_load(object sender, EventArgs e)
        {
            var port = SerialPort.GetPortNames();
            cboport.DataSource = port;

            if (string.IsNullOrEmpty(slvID_txtBox.Text))
               slvID_txtBox.Text = "17";
            if (string.IsNullOrEmpty(baud_comBox.Text))
                baud_comBox.Text = "38400";
            if(string.IsNullOrEmpty(dataBits_comBox.Text))
                dataBits_comBox.Text = "8";
            if(string.IsNullOrEmpty(parity_comBox.Text))
                parity_comBox.Text = "Even";
            if(string.IsNullOrEmpty(stopBits_comBox.Text))
                stopBits_comBox.Text = "1";
                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();

            Console.WriteLine("\n\n======================");
            Console.WriteLine("Checking COM Parameter:");
            Console.WriteLine("Slave ID: "+ slvID_txtBox.Text);
            Console.WriteLine("Port: " + cboport.Text);
            Console.WriteLine("Baud Rate : " + baud_comBox.Text);
            Console.WriteLine("Data Bits: " + dataBits_comBox.Text);
            Console.WriteLine("Parity : " + parity_comBox.Text);
            Console.WriteLine("Stop Bits: " + stopBits_comBox.Text);
            Console.WriteLine("======================\n\n");

        }
    }
}
