﻿
namespace KS_MV_SoftwareModbus
{
    partial class main_from
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main_from));
            this.label1 = new System.Windows.Forms.Label();
            this.isoCalc_cfg = new System.Windows.Forms.Button();
            this.comSetup_btn = new System.Windows.Forms.Button();
            this.about_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.deviceTime_btn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataLogView_btn = new System.Windows.Forms.Button();
            this.liveData_btn = new System.Windows.Forms.Button();
            this.ethernetIPforCLXCom1 = new AdvancedHMIDrivers.EthernetIPforCLXCom(this.components);
            this.button7 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetIPforCLXCom1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(317, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "KS-MV Soft";
            // 
            // isoCalc_cfg
            // 
            this.isoCalc_cfg.Location = new System.Drawing.Point(634, 325);
            this.isoCalc_cfg.Name = "isoCalc_cfg";
            this.isoCalc_cfg.Size = new System.Drawing.Size(154, 44);
            this.isoCalc_cfg.TabIndex = 5;
            this.isoCalc_cfg.Text = "Device Calculation Configuration";
            this.isoCalc_cfg.UseVisualStyleBackColor = true;
            this.isoCalc_cfg.Click += new System.EventHandler(this.isoCalc_cfg_Click);
            // 
            // comSetup_btn
            // 
            this.comSetup_btn.Location = new System.Drawing.Point(634, 275);
            this.comSetup_btn.Name = "comSetup_btn";
            this.comSetup_btn.Size = new System.Drawing.Size(154, 44);
            this.comSetup_btn.TabIndex = 6;
            this.comSetup_btn.Text = "COM Setup";
            this.comSetup_btn.UseVisualStyleBackColor = true;
            this.comSetup_btn.Click += new System.EventHandler(this.comSetup_btn_Click);
            // 
            // about_btn
            // 
            this.about_btn.Location = new System.Drawing.Point(235, 375);
            this.about_btn.Name = "about_btn";
            this.about_btn.Size = new System.Drawing.Size(154, 44);
            this.about_btn.TabIndex = 8;
            this.about_btn.Text = "About KS-MV Soft";
            this.about_btn.UseVisualStyleBackColor = true;
            this.about_btn.Click += new System.EventHandler(this.about_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(242, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(311, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Orifice Flow Meter Configurator and Data Viewer";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(336, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "For KS-MV1000";
            // 
            // deviceTime_btn
            // 
            this.deviceTime_btn.Location = new System.Drawing.Point(634, 375);
            this.deviceTime_btn.Name = "deviceTime_btn";
            this.deviceTime_btn.Size = new System.Drawing.Size(154, 44);
            this.deviceTime_btn.TabIndex = 11;
            this.deviceTime_btn.Text = "Device Time";
            this.deviceTime_btn.UseVisualStyleBackColor = true;
            this.deviceTime_btn.Click += new System.EventHandler(this.deviceTime_btn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(395, 375);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(154, 44);
            this.button2.TabIndex = 12;
            this.button2.Text = "FW Device About";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 275);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(136, 44);
            this.button3.TabIndex = 13;
            this.button3.Text = "Pressure Gauge";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 325);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(136, 44);
            this.button4.TabIndex = 14;
            this.button4.Text = "Difference Pressure";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(12, 375);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(136, 44);
            this.button5.TabIndex = 15;
            this.button5.Text = "Temperature RTD (3 Wire)";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 18);
            this.label4.TabIndex = 16;
            this.label4.Text = "Sensor Setting";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(653, 254);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 18);
            this.label5.TabIndex = 17;
            this.label5.Text = "Device Setting";
            // 
            // dataLogView_btn
            // 
            this.dataLogView_btn.Location = new System.Drawing.Point(12, 12);
            this.dataLogView_btn.Name = "dataLogView_btn";
            this.dataLogView_btn.Size = new System.Drawing.Size(154, 44);
            this.dataLogView_btn.TabIndex = 18;
            this.dataLogView_btn.Text = "Data Log View";
            this.dataLogView_btn.UseVisualStyleBackColor = true;
            this.dataLogView_btn.Click += new System.EventHandler(this.dataLogView_btn_Click);
            // 
            // liveData_btn
            // 
            this.liveData_btn.Location = new System.Drawing.Point(12, 62);
            this.liveData_btn.Name = "liveData_btn";
            this.liveData_btn.Size = new System.Drawing.Size(154, 44);
            this.liveData_btn.TabIndex = 19;
            this.liveData_btn.Text = "Live Data";
            this.liveData_btn.UseVisualStyleBackColor = true;
            this.liveData_btn.Click += new System.EventHandler(this.liveData_btn_Click);
            // 
            // ethernetIPforCLXCom1
            // 
            this.ethernetIPforCLXCom1.CIPConnectionSize = 508;
            this.ethernetIPforCLXCom1.DisableMultiServiceRequest = false;
            this.ethernetIPforCLXCom1.DisableSubscriptions = false;
            this.ethernetIPforCLXCom1.IniFileName = "";
            this.ethernetIPforCLXCom1.IniFileSection = null;
            this.ethernetIPforCLXCom1.IPAddress = "192.168.0.10";
            this.ethernetIPforCLXCom1.PollRateOverride = 500;
            this.ethernetIPforCLXCom1.Port = 44818;
            this.ethernetIPforCLXCom1.ProcessorSlot = 0;
            this.ethernetIPforCLXCom1.RoutePath = null;
            this.ethernetIPforCLXCom1.Timeout = 4000;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(634, 12);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(154, 44);
            this.button7.TabIndex = 21;
            this.button7.Text = "Calculate Simulation";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(323, 151);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(156, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // main_from
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 481);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.liveData_btn);
            this.Controls.Add(this.dataLogView_btn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.deviceTime_btn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.about_btn);
            this.Controls.Add(this.comSetup_btn);
            this.Controls.Add(this.isoCalc_cfg);
            this.Controls.Add(this.label1);
            this.Name = "main_from";
            this.Text = "KS-MV Soft";
            ((System.ComponentModel.ISupportInitialize)(this.ethernetIPforCLXCom1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button isoCalc_cfg;
        private System.Windows.Forms.Button comSetup_btn;
        private System.Windows.Forms.Button about_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button deviceTime_btn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button dataLogView_btn;
        private System.Windows.Forms.Button liveData_btn;
        private AdvancedHMIDrivers.EthernetIPforCLXCom ethernetIPforCLXCom1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

