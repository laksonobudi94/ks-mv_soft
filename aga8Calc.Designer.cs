﻿
namespace KS_MV_SoftwareModbus
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(353, 224);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(171, 17);
            this.label33.TabIndex = 89;
            this.label33.Text = "FR GC – N-Nonane (n-C9)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(346, 252);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(178, 17);
            this.label32.TabIndex = 88;
            this.label32.Text = "FR GC – N-Decane (n-C10)";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(354, 116);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(174, 17);
            this.label29.TabIndex = 87;
            this.label29.Text = "FR GC – N-Pentane (n-C5)";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(538, 139);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(64, 22);
            this.textBox22.TabIndex = 86;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(538, 111);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(64, 22);
            this.textBox21.TabIndex = 85;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(538, 83);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(64, 22);
            this.textBox20.TabIndex = 84;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(361, 194);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(167, 17);
            this.label28.TabIndex = 83;
            this.label28.Text = "FR GC – N-Octane (n-C8)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(353, 169);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(175, 17);
            this.label27.TabIndex = 82;
            this.label27.Text = "FR GC – N-Heptane (n-C7)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(359, 143);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(169, 17);
            this.label26.TabIndex = 81;
            this.label26.Text = "FR GC – N-Hexane (n-C6)";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(538, 55);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(64, 22);
            this.textBox19.TabIndex = 80;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(247, 335);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(64, 22);
            this.textBox18.TabIndex = 79;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(366, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(162, 17);
            this.label25.TabIndex = 78;
            this.label25.Text = "FR GC – I-Pentane (i-C5)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(358, 60);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(166, 17);
            this.label24.TabIndex = 77;
            this.label24.Text = "FR GC – N-Butane (n-C4)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(218, 9);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(248, 24);
            this.label23.TabIndex = 76;
            this.label23.Text = "Gas Component (Mole %)";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(247, 307);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(64, 22);
            this.textBox16.TabIndex = 75;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(247, 279);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(64, 22);
            this.textBox17.TabIndex = 74;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(79, 340);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(154, 17);
            this.label21.TabIndex = 73;
            this.label21.Text = "FR GC – I-Butane (i-C4)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(92, 312);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(143, 17);
            this.label22.TabIndex = 72;
            this.label22.Text = "FR GC – Oxygen (O2)";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(247, 251);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(64, 22);
            this.textBox14.TabIndex = 71;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(247, 223);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(64, 22);
            this.textBox15.TabIndex = 70;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(29, 284);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(206, 17);
            this.label19.TabIndex = 69;
            this.label19.Text = "FR GC – Carbon Monoxide (CO)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(29, 228);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(212, 17);
            this.label20.TabIndex = 68;
            this.label20.Text = "FR GC – Hydrogen Sulfide (H2S)";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(247, 195);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(64, 22);
            this.textBox12.TabIndex = 67;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(247, 167);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(64, 22);
            this.textBox13.TabIndex = 66;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(98, 200);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(143, 17);
            this.label17.TabIndex = 65;
            this.label17.Text = "FR GC – Water (H2O)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(94, 172);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(147, 17);
            this.label18.TabIndex = 64;
            this.label18.Text = "FR GC – Propane (C3)";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(247, 139);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(64, 22);
            this.textBox10.TabIndex = 63;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(247, 111);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(64, 22);
            this.textBox11.TabIndex = 62;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(103, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(138, 17);
            this.label15.TabIndex = 61;
            this.label15.Text = "FR GC – Ethane (C2)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(41, 116);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(200, 17);
            this.label16.TabIndex = 60;
            this.label16.Text = "FR GC – Carbon Dioxide (CO2)";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(247, 83);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(64, 22);
            this.textBox9.TabIndex = 59;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(247, 55);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(64, 22);
            this.textBox8.TabIndex = 58;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(93, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 17);
            this.label14.TabIndex = 57;
            this.label14.Text = "FR GC – Nitrogen (N2)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(93, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(148, 17);
            this.label13.TabIndex = 56;
            this.label13.Text = "FR GC – Methane (C1)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(538, 167);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(64, 22);
            this.textBox1.TabIndex = 91;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(538, 195);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(64, 22);
            this.textBox2.TabIndex = 92;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(538, 223);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(64, 22);
            this.textBox3.TabIndex = 93;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(396, 314);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(128, 17);
            this.label31.TabIndex = 95;
            this.label31.Text = "FR GC – Argon (Ar)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(384, 282);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(140, 17);
            this.label30.TabIndex = 94;
            this.label30.Text = "FR GC – Hellium (He)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 17);
            this.label1.TabIndex = 96;
            this.label1.Text = "FR GC – Hydrogen (H2)";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(538, 251);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(64, 22);
            this.textBox4.TabIndex = 97;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(538, 279);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(64, 22);
            this.textBox5.TabIndex = 98;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(538, 307);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(64, 22);
            this.textBox6.TabIndex = 99;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(272, 396);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 50);
            this.button1.TabIndex = 100;
            this.button1.Text = "Save Gas Component";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(479, 396);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 50);
            this.button2.TabIndex = 101;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 455);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Name = "Form4";
            this.Text = "Gas - Compressibility AGA8: 1994";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}